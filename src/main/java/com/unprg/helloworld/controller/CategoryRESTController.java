package com.unprg.helloworld.controller;

import com.unprg.helloworld.type.Category;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryRESTController {

    public static List<Category> categories;

    public CategoryRESTController() {
        categories = new ArrayList<>();
        //obj 1
        Category category1 = new Category();
        category1.setId("CAT01");
        category1.setName("ZAPATOS");
        categories.add(category1);
        //obj 2
        Category category2 = new Category();
        category2.setId("CAT02");
        category2.setName("CAMISAS");
        categories.add(category2);
    }

    @GetMapping
    public List<Category> list(){
        return categories;
    }

}
