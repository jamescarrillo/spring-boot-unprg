package com.unprg.helloworld.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("hello-world")
public class HelloWorldRESTController {

    @GetMapping
    public String saludo(){
        return "Hola amigos";
    }

}
